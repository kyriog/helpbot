# v0.1.0

* Answering to a message when at least a user react with a specific emoji
* Anti-spam feature: the bot will only answer once to a message

# v0.2.0

* The bot can now handle multiple emojis (and associated messages)
* The bot still only answer once per message, whatever the number of triggered emojis

# v0.5.0

* You can now filter allowed users using roles
* Adding channel whitelist/blacklist feature
* Reactor user is now logged both in database and logs
* Bot now deletes its message when the original message gets deleted

# v0.6.0

* Adding blacklisted users feature
