import logging
import sqlite3

from disco.bot import Plugin
from disco.api.http import APIException


class HelpbotPlugin(Plugin):
    db = None

    def load(self, ctx):
        super(HelpbotPlugin, self).load(ctx)
        self.db = sqlite3.connect('helpbot.db')
        cursor = self.db.cursor()
        cursor.execute("""
            CREATE TABLE IF NOT EXISTS messages(
                message_id INTEGER PRIMARY KEY UNIQUE,
                channel_id INTEGER,
                reactor_id INTEGER,
                bot_message_id INTEGER UNIQUE
            );
        """)
        self.db.commit()

    @Plugin.listen('MessageReactionAdd')
    def on_message_reaction_add(self, event):
        if event.user_id in self.config['blacklisted_users']:
            logging.info(
                "Banned user #{} has reacted emoji {} on message #{} (channel #{})".format(
                    event.user_id, event.emoji.name, event.message_id, event.channel_id
                )
            )
            return
        if event.emoji.name not in self.config['messages']:
            return
        if self.config['allowed_channels'] and event.channel_id not in self.config['allowed_channels']:
            return
        if event.channel_id in self.config['forbidden_channels']:
            return
        if self.config['allowed_roles']:
            answer_allowed = False
            reactor_user = event.channel.guild.members[event.user_id]
            for role in self.config['allowed_roles']:
                if role in reactor_user.roles:
                    answer_allowed = True
                    break
            if not answer_allowed:
                return

        # Everything is allowed, let's do the magic
        cursor = self.db.cursor()
        cursor.execute(
            "SELECT 1 FROM messages WHERE message_id=? AND channel_id=?",
            (event.message_id, event.channel_id)
        )
        if cursor.fetchone() is not None:
            return
        messages = self.state.messages[event.channel_id]
        author_id = None
        for message in messages:
            if message.id == event.message_id:
                author_id = message.author_id
                break
        if author_id is None:
            try:
                message = self.client.api.channels_messages_get(event.channel_id, event.message_id)
                author_id = message.author.id
            except APIException as e:
                if e.code == 10008:
                    return
                raise e
        if author_id == self.state.me.id:
            return
        bot_message = event.channel.send_message(self.config['messages'][event.emoji.name].format(author_id))
        logging.info(
            "#{} has reacted emoji {} on message #{} (channel #{})".format(
                event.user_id, event.emoji.name, event.message_id, event.channel_id
            )
        )
        cursor = self.db.cursor()
        cursor.execute(
            "INSERT INTO messages VALUES (?, ?, ?, ?)",
            (event.message_id, event.channel_id, event.user_id, bot_message.id)
        )
        self.db.commit()

    @Plugin.listen('MessageDelete')
    def on_message_delete(self, event):
        cursor = self.db.cursor()
        cursor.execute(
            "SELECT bot_message_id FROM messages WHERE message_id=? AND channel_id=?", (event.id, event.channel_id)
        )
        bot_message_id = cursor.fetchone()
        if bot_message_id:
            logging.info("Deleting reacted message #{} (channel #{})".format(event.id, event.channel_id))
            cursor.execute("DELETE FROM messages WHERE message_id=? AND channel_id=?", (event.id, event.channel_id))
            self.db.commit()
            bot_message_id = bot_message_id[0]
            try:
                self.client.api.channels_messages_delete(event.channel_id, bot_message_id)
            except APIException as e:
                if e.code != 10008:
                    raise e
